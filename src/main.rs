// Program entry point

// Call the command line parsing logic with the argument values
// Set up any other configuration
// Call a run function in lib.rs
// Handle the error if run returns an error
use minigrep::Config;
use std::env;
use std::process;

fn main() {
    // env::args will panic if any argument contains invalid Unicode
    // use env::args_os to accept invalid Unicode arguments
    // let args: Vec<String> = env::args().collect();

    // let config = Config::build(&args).unwrap_or_else(|err| {

    //  we’re passing ownership of the iterator returned from env::args to Config::build directly
    let config = Config::build(env::args()).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {err}");
        process::exit(1);
    });

    // println!("Searching for {}", config.query);
    // println!("In file {}", config.file_path);

    if let Err(e) = minigrep::run(config) {
        eprintln!("Application error: {e}");
        process::exit(1);
    }
}
